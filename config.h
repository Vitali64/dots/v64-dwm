/* See LICENSE file for copyright and license details. */


/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
//static const unsigned int gappx     = 10; /*gAPS i like*/
static const unsigned int gappx     = 3;
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int user_bh = 27; /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const unsigned int systraypinning = 0;
static const unsigned int systrayspacing = 2;
static const int systraypinningfailfirst = 1;
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showsystray = 1;
//static const char *fonts[]          = { "BigBlue_TerminalPlus Nerd Font:size=10" };
//static const char dmenufont[]       = "BigBlue_terminalPlus Nerd Font:size=10";
static const char *fonts[]          = { "JetBrains Mono Nerd Font Mono:size=11" };
static const char dmenufont[]       = "JetBrains Mono Nerd Font Mono:size=11";
static const char col_gray1[]       = "#1E1E1E";
static const char col_gray2[]       = "#262626";
static const char col_gray4[]       = "#ffffff";
static const char col_gray3[]       = "#bababa";
static const char col_cyan[]        = "#1E1E1E";

static const char *colors[][3] = {
	/*				 fg			bg		   border	*/
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray1,	col_cyan  },
};


/* tagging */
static const char *tags[] = {
	" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 ", " 10 " 
};
static const char *tagsalt[] = {
	"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class		   instance  title			 tags mask	isfloating	isterminal	noswallow  monitor */
	{ "Gimp",		   NULL,	 NULL,			 0,			1,			0,			 0,		   -1 },
	{ "Firefox",	   NULL,	 NULL,			 1 << 8,	0,			0,			-1,		   -1 },
	{ "st",			   NULL,	 NULL,			 0,			0,			1,			 0,		   -1 },
	{ NULL,			   NULL,	 "Event Tester", 0,			0,			0,			 1,		   -1 }, /* xev */
};

/* layout(s) */
static const float mfact	 = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster	 = 1;	 /* number of clients in master area */
static const int resizehints = 0;	 /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol	  arrange function */
	{ "[]=",	  tile },	 /* first entry is default */
	{ "><>",	  NULL },	 /* no layout function means floating behavior */
	{ "[M]",	  monocle },
	{ "|M|",	  centeredmaster },
	{ ">M>",	  centeredfloatingmaster },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,						KEY,	  view,			  {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,			KEY,	  toggleview,	  {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,				KEY,	  tag,			  {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,	  toggletag,	  {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/bash", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL }; /* DMENU (vitali64-dmenu is recommended) */
static const char *termcmd[]  = { "st", NULL }; /* Terminal (vitali64-st is recommended) */
static const char *showkeypresses[] = { "screenkey", NULL }; /* { OPTIONAL } screenkey */
static const char *layoutmenu_cmd = "layoutmenu.sh";
#define TERMINALRUN "st -e " /* Terminal to use for running terminal commands (vitali64-st is recommended) */
#define FILEMANAGER "ranger" /* File manager */
#define BROWSER "apulse tor-browser" /* Web browser */
#define WALLPAPERCHANGER "feh --bg-fill $(find ~/wallpapers -type f -iregex .*.jpg | sort -R | head -1)" /* Change the wallpaper by using feh */
#define EMAILCLIENT "neomutt" /* E-Mail client */
#define SCRIPTS "~/scripts" /* { OPTIONAL } My scripts directory */
static Key keys[] = {
	/* modifier						key		   function		   argument */
	{ MODKEY,						XK_p,	   spawn,		   {.v = dmenucmd } },
	{ MODKEY,						XK_Return, spawn,		   {.v = termcmd } },
	{ MODKEY,						XK_b,	   togglebar,	   {0} },
	{ MODKEY,						XK_j,	   focusstack,	   {.i = +1 } },
	{ MODKEY,						XK_k,	   focusstack,	   {.i = -1 } },
	{ MODKEY,						XK_i,	   incnmaster,	   {.i = +1 } },
	{ MODKEY,						XK_d,	   incnmaster,	   {.i = -1 } },
	{ MODKEY,						XK_h,	   setmfact,	   {.f = -0.05} },
	{ MODKEY,						XK_l,	   setmfact,	   {.f = +0.05} },
	{ MODKEY|ShiftMask,				XK_Return, zoom,		   {0} },
	{ MODKEY,						XK_Tab,    view,		   {0} },
	{ MODKEY|ShiftMask,				XK_c,	   killclient,	   {0} },
	{ MODKEY,						XK_t,	   setlayout,	   {.v = &layouts[0]} },
	{ MODKEY,						XK_f,	   setlayout,	   {.v = &layouts[1]} },
	{ MODKEY,						XK_m,	   setlayout,	   {.v = &layouts[2]} },
	{ MODKEY,						XK_u,	   setlayout,	   {.v = &layouts[3]} },
	{ MODKEY,						XK_o,	   setlayout,	   {.v = &layouts[4]} },
	{ MODKEY,						XK_space,  setlayout,	   {0} },
	{ MODKEY|ShiftMask,				XK_space,  togglefloating, {0} },
	{ MODKEY,						XK_a,	   view,		   {.ui = ~0 } },
	{ MODKEY|ShiftMask,				XK_a,	   tag,			   {.ui = ~0 } },
	{ MODKEY,						XK_comma,  focusmon,	   {.i = -1 } },
	{ MODKEY,						XK_period, focusmon,	   {.i = +1 } },
	{ MODKEY|ShiftMask,				XK_comma,  tagmon,		   {.i = -1 } },
	{ MODKEY|ShiftMask,				XK_period, tagmon,		   {.i = +1 } },
	{ MODKEY|ShiftMask,				XK_f,	   togglefullscr,  {0} },
	{ MODKEY,						XK_n,	   togglealttag,   {0} },
	/* 
	 * Here goes our own bindings for running an app.
	 * For some bindings, you need to remove "TERMINALRUN"
	 * if the program is a GUI program (CLI program by default) 
	 * to prevent an unnecessary terminal to pop up.
	 */
	{ MODKEY|ControlMask,			XK_Return, spawn,		   SHCMD(TERMINALRUN FILEMANAGER) },
	{ MODKEY|ControlMask,			XK_m,	   spawn,		   SHCMD(TERMINALRUN EMAILCLIENT) },
	{ MODKEY|ControlMask,			XK_p,	   spawn,		   {.v = showkeypresses} },
	{ MODKEY|ControlMask,			XK_space,  spawn,		   SHCMD(BROWSER) },
	{ MODKEY|ControlMask,			XK_Tab,    spawn,		   SHCMD(WALLPAPERCHANGER) },
	{ MODKEY|ShiftMask,				XK_h,	   setcfact,	   {.f = +0.25} },
	{ MODKEY|ShiftMask,				XK_l,	   setcfact,	   {.f = -0.25} },
	{ MODKEY|ShiftMask,				XK_o,	   setcfact,	   {.f =  0.00} },
	TAGKEYS(						XK_1,					   0)
	TAGKEYS(						XK_2,					   1)
	TAGKEYS(						XK_3,					   2)
	TAGKEYS(						XK_4,					   3)
	TAGKEYS(						XK_5,					   4)
	TAGKEYS(						XK_6,					   5)
	TAGKEYS(						XK_7,					   6)
	TAGKEYS(						XK_8,					   7)
	TAGKEYS(						XK_9,					   8)
	TAGKEYS(						XK_0,					   9)
	{ MODKEY|ShiftMask,				XK_q,	   quit,		   {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click				event mask		button			function		argument */
	{ ClkLtSymbol,			0,				Button1,		setlayout,		{0} },
	/*{ ClkLtSymbol,		  0,			  Button3,		  setlayout,	  {.v = &layouts[2]} },*/
	{ ClkLtSymbol,			0,				Button3,		layoutmenu,		{0} },
	{ ClkWinTitle,			0,				Button2,		zoom,			{0} },
	{ ClkStatusText,		0,				Button2,		spawn,			{.v = termcmd } },
	{ ClkClientWin,			MODKEY,			Button1,		movemouse,		{0} },
	{ ClkClientWin,			MODKEY,			Button2,		togglefloating, {0} },
	{ ClkClientWin,			MODKEY,			Button3,		resizemouse,	{0} },
	{ ClkTagBar,			0,				Button1,		view,			{0} },
	{ ClkTagBar,			0,				Button3,		toggleview,		{0} },
	{ ClkTagBar,			MODKEY,			Button1,		tag,			{0} },
	{ ClkTagBar,			MODKEY,			Button3,		toggletag,		{0} },
};

