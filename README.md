# v64-dwm - Vitali64's dynamic window manager build
dwm is an extremely fast, small, and dynamic window manager for X.

<img src="./Screenshot.png">

*conky, dwmblocks, picom configs and wallpaper AREN'T included*

# NEW !!!!
This fork is now available in the AUR ! 

    yay -S vitali64-dwm-git



# Requirements

In order to build dwm you need the Xlib header files and GNU/Linux or BSD (FreeBSD, OpenBSD, ...) with Xorg installed.
It doesn't and will not work on other systems such as macOS and Windows because this relies on libraries available exclusively on Linux.

This has been tested on Artix Linux (runit) but should work with any other distro such as Ubuntu Linux


Installation
------------
Edit `config.mk` to match your local setup (most of the time, the `config.mk` provided here should work without any modification).

Afterwards enter the following command to build (if
necessary as root):

    make

And if you want to install it on the system (Ehh, which I don't!), run the following :

    sudo make clean install

Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec <where the binary file is located>/dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm

# Patches

- actualfullscreen : fullscreens the window ( <kb>Super</kb> + <kb>Shift</kb> + <kb>f</kb>)

- centeredmaster : A layout where the master window is always centered (Super + u/o)

- layoutmenu (subject to be removed)

- cfacts : Better support of window resizing (Super + Shift + h/l)

- fixborders : Disable transparent borders

- swallow : When a GUI app is launched from the terminal, make the terminal dissapear for saving screen space

- systray : Does exactly what the name says

- uselessgap : Gaps on all layouts except the Monocle layout

- hide_vacant-tags : Hides unused tags like i3 does

- alternativetags : alternative tag names (Super + n)
